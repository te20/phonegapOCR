/*jshint devel: true, strict: false*/
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
/*
var onSuccess = function (heading) {
    var  element = document.getElementById('heading');
    element.innerHTML = 'Heading: ' + heading.magneticHeading;
};

var onError = function (compassError) {
    alert('Compass error: ' + compassError.code);
};
*/
function OCRImage(image) {
    console.log('OCR start');
    var canvas = document.createElement('canvas');
    canvas.width = image.naturalWidth;
    canvas.height = image.naturalHeight;
    canvas.getContext('2d').drawImage(image, 0, 0);

    // var element = document.getElementById('OCR');
    // element.innerHTML = canvas;

    var string = OCRAD(canvas);
    console.log('OCR result: ' + string);
    return string;
}

function onSuccess(imageData) {
    var image = document.getElementById('myImage');
    image.src = 'data:image/jpeg;base64,' + imageData;
    // return;

    var ocrImage = new Image();
    ocrImage.src = 'data:image/jpeg;base64,' + imageData;
    ocrImage.onload = function() {
        writeOCRText(OCRImage(ocrImage));
    };
}

function writeOCRText(string) {
    var element = document.getElementById('output');
    element.innerHTML = string;
}

function onFail(message) {
    alert('Failed because: ' + message);
}

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        /*
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
        */
        navigator.vibrate(100);
        console.log('start app');
        // var watchID = navigator.compass.watchHeading(onSuccess, onError, options);
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            targetWidth: 200,
            targetHeight: 400,
            // encodingType: Camera.EncodeingType.PNG,
            destinationType: Camera.DestinationType.DATA_URL
        });
    }
};
